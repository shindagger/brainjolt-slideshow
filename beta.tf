terraform {
  backend "s3" {
    bucket  = "brainjolt-slideshow"
    key     = "terraform.tfstate"
    region  = "us-east-1"
    encrypt = true
  }
}

