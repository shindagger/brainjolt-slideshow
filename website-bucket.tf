locals {
  s3_origin_id = "brainjolt-slideshow-hosted"
  deploy_cmd = "./deploy brainjolt-slideshow-hosted"
}

resource "null_resource" "init" {
    provisioner "local-exec" {
        command = "yarn build"
    }
    triggers = {
        build_number = timestamp()
    }
    depends_on = [aws_s3_bucket.brainjolt_slideshow_bucket]
}

data "archive_file" "main_dir_zip" {
  type        = "zip"
  source_dir = "./build"
  output_path = "./build.zip"
  depends_on = [aws_s3_bucket.brainjolt_slideshow_bucket, null_resource.init]
}

# website bucket with logs

resource "aws_s3_bucket" "brainjolt_slideshow_bucket" {
  force_destroy = true
  bucket = local.s3_origin_id
  acl    = "public-read"
  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["GET", "PUT", "POST", "HEAD"]
    allowed_origins = ["*"]
    max_age_seconds = 3000
  }
  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {   
            "Sid": "PublicReadGetObject",
            "Effect": "Allow",
            "Principal": "*",
            "Action": "s3:GetObject",
            "Resource": "arn:aws:s3:::${local.s3_origin_id}/*"
        }   
    ]   
}
  POLICY
  website {
    index_document = "index.html"
    error_document = "index.html"
  }
}

resource "null_resource" "deploy_website" {
  provisioner "local-exec" {
    command = local.deploy_cmd
  }
  triggers = {
    build_number = timestamp()
  }
  depends_on = [aws_s3_bucket.brainjolt_slideshow_bucket, data.archive_file.main_dir_zip, null_resource.init]
}

output "website-bucket" {
  value = aws_s3_bucket.brainjolt_slideshow_bucket.website_endpoint
}

