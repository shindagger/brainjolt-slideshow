import React from 'react';
import styled from 'styled-components';
import { Wrapper, Content, Center, BigSpace, Navbar, GlobalTitle } from './Elements';
import one from '../img/1.jpg';
import two from '../img/2.jpg';
import three from '../img/3.jpg';
import four from '../img/4.jpg';
import five from '../img/5.jpg';
import six from '../img/6.jpg';


const Descrip = styled.div`
    display:inline-block;
    color:#999999;
    @media (max-width: 560px) {
        display:block;
        margin-top:-24px;
    }
    @media (max-width:330px) {
        margin-top:-18px;
    }
`;

const SlideHolder = styled.div`
    position:relative;
    @media (max-width: 600px) {
        width:100%;
    }
`;

const ImageHolder = styled.div`
    display:inline-block;
`;

const NextButton = styled.button`
    background-color:white;
    width:90px;
    font-family: 'Playfair Display', serif;
    font-size:1.3em;
    position:absolute;
    bottom:30px;
    left:50%;
    margin: 1em;
    padding: 0.25em 1em;
    border-radius: 0px;
    cursor: pointer;
    -webkit-transform: translateX(-50%);
    transform: translateX(-50%)
    outline: none;
`;

const Headline = styled.div`
    background-color:white;
    padding:20px;
    width:200px;
    position:absolute;
    top:30px;
    left:50%;
    -webkit-transform: translateX(-50%);
    transform: translateX(-50%)
`;

const Image = styled.img`
    @media (max-width: 600px) {
        width:100%;
    }
`;

class Slide extends React.Component {
    render() { 
        return (
            <SlideHolder>
                <ImageHolder><Image src={this.props.source} /></ImageHolder>
                <Headline>{this.props.headline}</Headline>
                <NextButton onClick={this.props.newImage}>Next</NextButton>
            </SlideHolder>
        );
    }
}

class Home extends React.Component {
    state = {
        "og":[
            {"image":one, "headline":"A Dog with Gloves On."}, 
            {"image":two, "headline":"A Tree with Underwear On."}, 
            {"image":three, "headline":"A Girl With No Legs"}, 
            {"image":four, "headline":"Girl with Super Long Legs"}, 
            {"image":five, "headline":"Strange Life Jacket"}, 
            {"image":six, "headline":"Bird in a Glass of Ice Water"}
        ],
        "images":[
            {"image":one, "headline":"A Dog with Gloves On."}, 
            {"image":two, "headline":"A Tree with Underwear On."}, 
            {"image":three, "headline":"A Girl With No Legs"}, 
            {"image":four, "headline":"Girl with Super Long Legs"}, 
            {"image":five, "headline":"Strange Life Jacket"}, 
            {"image":six, "headline":"Bird in a Glass of Ice Water"}
        ],
        "random":{},
        "last":{}
    }
    componentDidMount = () => {
        let random_arr_item = this.state.images[Math.floor(Math.random() * this.state.images.length)]
        let new_images = this.state.images.filter(image => image !== random_arr_item)
        this.setState({
            "random":random_arr_item,
            "images":new_images
        })
    }
    newImage = async () => {
        await this.setState({"last":this.state.random})
        if (this.state.images.length === 0) {
            await this.setState({
                "images":this.state.og
            });
            console.log("new images!")
        }
        let random_arr_item = this.state.images[Math.floor(Math.random() * this.state.images.length)]
        while (random_arr_item === this.state.last) {
            let random_arr_item = this.state.images[Math.floor(Math.random() * this.state.images.length)]
        }
        let new_images = this.state.images.filter(image => image !== random_arr_item)
        this.setState({
            "random":random_arr_item,
            "images":new_images
        })
    }
    render() {
        console.log(this.state.random)
        return (
            <Wrapper>
                <Content>
                    <Center>
                        <GlobalTitle title="Slideshow" large inline /><Descrip>of random images</Descrip>
                        <Slide source={this.state.random.image} headline={this.state.random.headline} newImage={this.newImage} />        
                    </Center>
                </Content>
            </Wrapper>
        );
    }
}

export {Home}
