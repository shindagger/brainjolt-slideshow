import React from 'react'; 
import styled from 'styled-components';
import { Link } from 'react-router-dom';

const Title = styled.h1`
    font-family: 'Lato', sans-serif;
    font-size: ${props => props.large ? '2.4em;' : '1.5em'};
    color: #000000;
    display: ${props => props.inline ? 'inline-block' : 'block'};
    margin-right: ${props => props.inline ? '5px' : '0px'};
    margin-top: 8px;
    @media (max-width: 600px) {
        font-size: ${props => props.large ? '1.9em;' : '1.5em'};
    }
    @media (max-width: 330px) {
        font-size: ${props => props.large ? '1.4em;' : '1.4em'};
    }
`;

const Wrapper = styled.div`
    position:relative;
    width:100vw;
    height:100vh;
    padding-bottom:40px;
    background-color: ${props => props.grey ? '#eeeeee' : '#ffffff'};
`;

const Content = styled.div`
    margin: auto;
    max-width: 1000px;
    font-size: 1em;
    clear:both;
    @media (max-width: 1011px) {
        padding-left:6px;
        padding-right:6px;
    }
`;

const Nav = styled.nav`
    background-color: #cfcfcf;
    box-shadow: 0 2px 2px 0px rgba(153, 153, 153, .8);
    -moz-box-shadow: 0 2px 2px 0px rgba(153, 153, 153, .8);
    -webkit-box-shadow: 0 2px 2px 0px rgba(153, 153, 153, .8);
    min-height: 60px;
    padding-top:5px;
    padding-bottom:5px;
    width:100vw;
    color: white;
    position:fixed;
    top:0;
    text-align: center;
    z-index:99;
`;

const LogoHolder = styled.div`
    display:inline-block;
    margin-top:10px;
`;

const Logo = styled.div`
    display:inline-block;
    width:40px;
    height:40px;
    background-color:red;
    border-radius:5px;
`;

const Center = styled.div`
    text-align: center;
    width:100%;
`;

const BigSpace = styled.div`
    height: ${props => props.loaded ? '185px' : '75px'};
    @media (max-width: 600px) {
        height: ${props => props.loaded ? '170px' : '75px'};
    }
    @media (max-width: 560px) {
        height: ${props => props.loaded ? '195px' : '75px'};
    }
    @media (max-width: 330px) {
        height: ${props => props.loaded ? '184px' : '75px'};
    }
`;

class GlobalTitle extends React.Component {
    render(title) {
        var large = {};
        var inline = {};
        if (this.props.large !== undefined) {
            large['large'] = "large";
        }
        if (this.props.inline !== undefined) {
            inline['inline'] = "inline";
        }
        return (
            <Title {...large} {...inline}>{this.props.title}</Title>
        )
    }
}

class Navbar extends React.Component {
    render() {
        return (
            <Nav>
                <Link to="/">
                <LogoHolder>
                    <Logo />
                </LogoHolder>
                </Link>
            </Nav>
        )   
    }   
}

export {Wrapper}
export {Content}
export {BigSpace}
export {Center}
export {GlobalTitle}
export {Navbar}
